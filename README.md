# Project Euler

## Setup

Create local virtual environment:

```bash
bazel run :create_venv
```

## Update requirements

Edit `requirements.in`, then run:

```bash
bazel run :generate_requirements_txt
```
