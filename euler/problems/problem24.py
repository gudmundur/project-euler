from itertools import permutations

import fire
from tqdm import tqdm


def main() -> int:
  n = 10
  for i, combo in tqdm(enumerate(permutations(map(str, range(n)), n)), total=1e6 - 1):
    if i == 999_999:
      return int("".join(combo))
  return -1


if __name__ == "__main__":
  fire.Fire(main)
