import math
from typing import cast

import fire


def find_abc(n: int) -> tuple[int, int, int]:
  for a in reversed(range(1, n // 2 - 1)):
    top = n * (n - 2 * a)
    bottom = 2 * (n - a)
    if (top % bottom) == 0:
      b = top // bottom
      c = round(math.sqrt(a**2 + b**2))
      return cast(tuple[int, int, int], tuple(sorted([a, b, c])))
  return -1, -1, -1


def main(n: int = 1000) -> int:
  a, b, c = find_abc(n)
  print(a, b, c)
  if a != -1:
    return a * b * c
  raise RuntimeError("failed to find solution")


if __name__ == "__main__":
  fire.Fire(main)
