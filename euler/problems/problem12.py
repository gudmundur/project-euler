import fire

from euler.primes.cython import eratosthenes


def get_n_divisors(factors: dict[int, int]) -> int:
  divisors = 1
  for multiplicity in factors.values():
    divisors *= multiplicity + 1
  return divisors


def get_prime_factors(n: int, primes: list[int]) -> dict[int, int]:
  factors: dict[int, int] = {}
  n = abs(n)
  primes_iter = iter(primes)
  prime = next(primes_iter)
  while n != 1:
    while (n % prime) == 0:
      factors[prime] = factors.get(prime, 0) + 1
      n //= prime
    prime = next(primes_iter)
  return factors


def main() -> int:
  primes = eratosthenes.get_n_primes(100_000)
  n = 3
  n_divisors = 1
  N = 2
  while n_divisors <= 500:
    if (n % 2) == 0:
      odd = n + 1
      even = n // 2
    else:
      odd = n
      even = (n + 1) // 2
    even_factors = get_prime_factors(even, primes)
    odd_factors = get_prime_factors(odd, primes)
    n_divisors = get_n_divisors(even_factors) * get_n_divisors(odd_factors)
    N = odd * even
    n += 1
  return N


if __name__ == "__main__":
  fire.Fire(main)
