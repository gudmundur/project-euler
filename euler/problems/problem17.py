import fire

NUMS = {
  1: "one",
  2: "two",
  3: "three",
  4: "four",
  5: "five",
  6: "six",
  7: "seven",
  8: "eight",
  9: "nine",
  10: "ten",
  11: "eleven",
  12: "twelve",
  13: "thirteen",
  14: "fourteen",
  15: "fifteen",
  16: "sixteen",
  17: "seventeen",
  18: "eighteen",
  19: "nineteen",
  20: "twenty",
  30: "thirty",
  40: "forty",
  50: "fifty",
  60: "sixty",
  70: "seventy",
  80: "eighty",
  90: "ninety",
}


def num2str(n: int) -> str:
  if n == 0:
    return ""
  if n > 1000 or n < 1:
    raise RuntimeError("out of bounds")
  if n in NUMS:
    return NUMS[n]
  match n:
    case n if n == 1000:
      return "onethousand"
    case n if n >= 100:
      remainder = num2str(n - 100 * (n // 100))
      remainder = "and" + remainder if remainder else ""
      return NUMS[n // 100] + "hundred" + remainder
    case n if n >= 20:
      return NUMS[10 * (n // 10)] + num2str(n - 10 * (n // 10))
    case _:
      raise RuntimeError("unreachable")


def main(n: int = 1000) -> int:
  return sum(len(num2str(i)) for i in range(1, n + 1))


if __name__ == "__main__":
  fire.Fire(main)
