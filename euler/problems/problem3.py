import math

import fire


def eratosthenes(n: int) -> list[int]:
  n = abs(n)
  if n < 2:
    return []
  if n == 2:
    return [2]
  primes = [2]
  current = 3
  while current <= n:
    if not any((current % p) == 0 for p in primes):
      primes.append(current)
    current += 2
  return primes


def main(n: int = 600851475143):
  potential_factors = eratosthenes(int(math.sqrt(n)))
  for factor in reversed(potential_factors):
    if (n % factor) == 0:
      return factor


if __name__ == "__main__":
  fire.Fire(main)
