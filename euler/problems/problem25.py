import math

import fire


def main(n: int = 1000) -> int:
  phi = (1 + math.sqrt(5)) / 2
  return math.ceil(((n - 1) + math.log10(math.sqrt(5))) / math.log10(phi))


if __name__ == "__main__":
  fire.Fire(main)
