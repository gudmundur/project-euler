from math import factorial

import fire


def main(n: int = 20) -> int:
  return factorial(2 * n) // (factorial(n) ** 2)


if __name__ == "__main__":
  fire.Fire(main)
