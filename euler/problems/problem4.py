from itertools import combinations_with_replacement

import fire


def is_palindrome(n: int) -> bool:
  s = str(n)
  for i in range(len(s) // 2):
    if s[i] != s[-(i + 1)]:
      return False
  return True


def main() -> int:
  palindromes: list[int] = []
  for left, right in combinations_with_replacement(reversed(range(100, 1000)), 2):
    prod = left * right
    if is_palindrome(prod):
      palindromes.append(prod)
  return max(palindromes)


if __name__ == "__main__":
  fire.Fire(main)
