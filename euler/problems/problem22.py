import re

import fire
from python.runfiles import Runfiles

NAME_PAT = re.compile(r'"(\w+)"')


def alphabet_rank(char: str) -> int:
  rank = ord(char) - ord("A") + 1
  if rank <= 0 or rank > ord("Z"):
    raise RuntimeError(f"not alphabetical {char}")
  return rank


def alphabet_value(s: str) -> int:
  return sum(map(alphabet_rank, s))


def main() -> int:
  r = Runfiles.Create()
  if r is None:
    raise RuntimeError("failed to create runfiles")
  loc = r.Rlocation("_main/euler/problems/data/names.txt")
  if loc is None:
    raise RuntimeError("failed to find location")
  with open(loc, "r") as f:
    contents = f.read()
  names: list[str] = []
  for match in NAME_PAT.finditer(contents):
    names.append(match.group(1))
  sum_ = 0
  for idx, name in enumerate(sorted(names)):
    rank = idx + 1
    sum_ += rank * alphabet_value(name)
  return sum_


if __name__ == "__main__":
  fire.Fire(main)
