import fire


def eratosthenes(n: int) -> int:
  if n <= 1:
    return 2
  primes = [2]
  idx = 2
  current = 3
  while idx <= n:
    if not any((current % p) == 0 for p in primes):
      idx += 1
      primes.append(current)
    current += 2
  return primes[-1]


def main(n: int = 10_001) -> int:
  return eratosthenes(n)


if __name__ == "__main__":
  fire.Fire(main)
