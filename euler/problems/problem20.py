import math

import fire


def main() -> int:
  return sum(map(int, str(math.factorial(100))))


if __name__ == "__main__":
  fire.Fire(main)
