import fire


def main(n: int = 1000) -> int:
  return sum(i for i in range(1, n) if (i % 3 == 0) or (i % 5 == 0))


if __name__ == "__main__":
  fire.Fire(main)
