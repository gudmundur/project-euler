import math

import fire
import numpy as np

data = """\
  75
  95 64
  17 47 82
  18 35 87 10
  20 04 82 47 65
  19 01 23 75 03 34
  88 02 77 73 07 63 67
  99 65 04 28 06 16 70 92
  41 41 26 56 83 40 80 70 33
  41 48 72 33 47 32 37 16 94 29
  53 71 44 65 25 43 91 52 97 51 14
  70 11 33 28 77 73 17 78 39 68 17 57
  91 71 52 38 17 14 91 43 58 50 27 29 48
  63 66 04 68 89 53 67 30 73 16 69 87 40 31
  04 62 98 27 23 09 70 98 73 93 38 53 60 04 23\
  """


def main() -> int:
  numbers = list(map(int, data.split()))
  numrows = round((math.sqrt(8 * len(numbers) + 1) - 1) / 2)
  array = np.zeros((numrows, numrows), dtype=np.int64)
  mask = np.tri(numrows, dtype=np.bool)
  array[mask] = numbers
  col_size = 1
  for row_idx in range(1, numrows):
    col_size += 1
    array[row_idx, 0] += array[row_idx - 1, 0]
    array[row_idx, col_size - 1] += array[row_idx - 1, col_size - 2]
    for col_idx in range(1, col_size - 1):
      value = array[row_idx, col_idx]
      parent1 = array[row_idx - 1, col_idx]
      parent2 = array[row_idx - 1, col_idx - 1]
      array[row_idx, col_idx] = max(value + parent1, value + parent2)
  return max(array[numrows - 1, :])


if __name__ == "__main__":
  fire.Fire(main)
