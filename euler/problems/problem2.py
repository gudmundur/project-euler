import fire


def main(n: int = 4_000_000) -> int:
  current = 1
  previous = 0
  sum_ = 0
  while current < n:
    if (current % 2) == 0:
      sum_ += current
    previous, current = current, current + previous
  return sum_


if __name__ == "__main__":
  fire.Fire(main)
