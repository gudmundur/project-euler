from itertools import combinations_with_replacement

import fire
from tqdm import tqdm

from euler.primes.cython import eratosthenes


def main() -> int:
  primes = eratosthenes.get_primes_less_than_n(100_000)
  abundants: list[int] = [
    n for n in tqdm(range(2, 28_124)) if n < eratosthenes.get_sum_of_proper_divisors(n, primes)
  ]
  candidates = set(range(1, 28_124))
  for left, right in tqdm(list(combinations_with_replacement(abundants, 2))):
    candidates -= {left + right}
  return sum(candidates)


if __name__ == "__main__":
  fire.Fire(main)
