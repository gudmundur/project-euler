import fire


def main(n: int = 100) -> int:
  sum_ = sum(range(1, n + 1))
  square_of_sum = sum_**2
  sum_of_square = sum(i**2 for i in range(1, n + 1))
  return abs(square_of_sum - sum_of_square)


if __name__ == "__main__":
  fire.Fire(main)
