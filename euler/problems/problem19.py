from datetime import date
from itertools import product

import fire


def main() -> int:
  sundays = 0
  for year, month in product(range(1901, 2001), range(1, 13)):
    sundays += date(year, month, 1).weekday() == 6
  return sundays


if __name__ == "__main__":
  fire.Fire(main)
