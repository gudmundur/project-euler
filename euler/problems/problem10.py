import fire

from euler.primes.cython import eratosthenes


def main(n: int = 2_000_000) -> int:
  prime = eratosthenes.get_primes_less_than_n(n)
  return sum(prime)


if __name__ == "__main__":
  fire.Fire(main)
