import fire
from tqdm import tqdm

from euler.primes.cython import eratosthenes


def main() -> int:
  primes = eratosthenes.get_primes_less_than_n(10_001)
  sums: dict[int, int] = {1: 1}
  for number in tqdm(range(2, 10_000)):
    sums[number] = eratosthenes.get_sum_of_proper_divisors(number, primes)
  amical_numbers: set[int] = set()
  for left, right in sums.items():
    if left == right or left > 10_000 or right > 10_000:
      continue
    if left == sums[right]:
      amical_numbers |= {left, right}
  return sum(amical_numbers)


if __name__ == "__main__":
  fire.Fire(main)
