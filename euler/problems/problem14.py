from functools import cache

import fire
from tqdm import tqdm


def collatz_iteration(n: int) -> int:
  if (n % 2) == 0:
    return n // 2
  return 3 * n + 1


@cache
def collatz_length(n: int) -> int:
  if n == 1:
    return 0
  return 1 + collatz_length(collatz_iteration(n))


def main(n: int = 1_000_000) -> int:
  longest_chain = 0
  longest_n = 1
  for start in tqdm(range(1, n)):
    chain_length = collatz_length(start)
    if chain_length > longest_chain:
      longest_chain = chain_length
      longest_n = start
  return longest_n


if __name__ == "__main__":
  fire.Fire(main)
