import fire


def main() -> int:
  return sum(map(int, str(2**1000)))


if __name__ == "__main__":
  fire.Fire(main)
