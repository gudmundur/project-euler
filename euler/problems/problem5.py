import fire


def is_divisible(n: int, divisors: list[int]) -> bool:
  return all((n % div) == 0 for div in divisors)


def main(n: int = 20) -> int:
  current = 21
  divisors = list(reversed(range(2, n + 1)))
  while not is_divisible(current, divisors):
    current += 1
  return current


if __name__ == "__main__":
  fire.Fire(main)
