from functools import reduce
from itertools import combinations
from operator import mul

import cython
from libcpp.vector cimport vector

@cython.cdivision(True)
cdef bint is_prime(const long n, const vector[long] &primes):
    cdef long prime
    for prime_idx in range(primes.size()):
        prime = primes[prime_idx]
        if prime * prime > n:
            break
        if n % prime == 0:
            return <bint> False
    return <bint> True

cdef int _get_first_n_primes(const size_t n, vector[long] &primes):
    cdef long candidate
    primes.push_back(<long> 2)
    candidate = <long> 1
    while primes.size() < n:
        candidate += 2
        if is_prime(candidate, primes):
            primes.push_back(candidate)

cdef int _get_primes_less_than_n(const size_t n, vector[long] &primes):
    cdef long candidate
    primes.push_back(<long> 2)
    candidate = <long> 1
    while candidate < n:
        candidate += 2
        if is_prime(candidate, primes):
            primes.push_back(candidate)

@cython.cdivision(True)
cdef long _sum_of_proper_divisors(const long n, const vector[long] &primes):
    cdef long prime, m
    prime_divisors: list[int] = []
    m = n
    for idx in range(primes.size()):
        prime = primes[idx]
        if prime > m:
            break
        while (m % prime) == 0:
            m //= prime
            prime_divisors.append(int(prime))
    cdef long prime1, prime2, divisor, sum_
    sum_ = <long> 0
    proper_divisors: set[int] = set()
    for divisor_size in range(1, len(prime_divisors)):
        for divisors in combinations(prime_divisors, divisor_size):
            proper_divisor = reduce(mul, divisors, 1)
            if proper_divisor not in proper_divisors:
                sum_ += reduce(mul, divisors, 1)
                proper_divisors.add(proper_divisor)
    return 1 + sum_

def get_nth_prime(n: int) -> int:
    cdef vector[long] primes
    _get_first_n_primes(<size_t>n, primes)
    ret = primes.back()
    return ret

def get_n_primes(n: int) -> list[int]:
    cdef vector[long] primes
    _get_first_n_primes(<size_t>n, primes)
    return [int(primes[i]) for i in range(primes.size())]

def get_primes_less_than_n(n: int) -> list[int]:
    cdef vector[long] primes
    _get_primes_less_than_n(<size_t> n, primes)
    return [int(primes[i]) for i in range(primes.size())]

def get_sum_of_proper_divisors(n: int, primes: list[int]) -> int:
    cdef vector[long] primes_c
    for prime in primes:
        primes_c.push_back(<long>prime)
    return int(_sum_of_proper_divisors(<long> n, primes_c))