from euler.primes.cython import eratosthenes_cython  # type: ignore


def get_nth_prime(n: int) -> int:
  return eratosthenes_cython.get_nth_prime(n)


def get_primes_less_than_n(n: int) -> list[int]:
  return eratosthenes_cython.get_primes_less_than_n(n)


def get_n_primes(n: int) -> list[int]:
  return eratosthenes_cython.get_n_primes(n)


def get_sum_of_proper_divisors(n: int, primes: list[int]) -> int:
  return eratosthenes_cython.get_sum_of_proper_divisors(n, primes)
